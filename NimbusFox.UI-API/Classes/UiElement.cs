﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Plukit.Base;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Logic;

namespace NimbusFox.UI_API.Classes {
    public abstract class UiElement : IDisposable {
        public abstract void Dispose();

        protected UiWindow Window { get; }
        protected UiElement Parent { get; }

        public UiElement(UiWindow window, UiElement parent = null) {
            Window = window;
            Parent = parent;
        }

        protected List<UiElement> Elements { get; } = new List<UiElement>();

        public abstract void Draw(DeviceContext graphics, Entity entity, Universe universe, Vector2 origin, SpriteBatch spriteBatch);

        public virtual Vector2 GetSize() {
            var size = new Vector2(0, 0);

            foreach (var element in Elements) {
                var eleSize = element.GetSize();

                size.X += eleSize.X;
                size.Y += eleSize.Y;
            }

            return size;
        }

        public abstract void Update(Universe universe, Entity entity);

        public void AddChild(UiElement element) {
            Elements.Add(element);
        }
    }
}
