﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.FoxCore.V3;
using NimbusFox.FoxCore.V3.Patches;
using Plukit.Base;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.UI_API.Patches {
    public static class OverlayRendererPatches {
        internal static void Initialize() {
            UIHook.PatchController.Add(typeof(OverlayRenderer), "Draw", null, null, typeof(OverlayRendererPatches), "Draw");
            UIHook.PatchController.Add(typeof(OverlayRenderer), "DrawTop", typeof(OverlayRendererPatches), "DrawTop");
        }

        private static void Draw(DeviceContext graphics, ref Matrix4F matrix, Entity avatar, Universe universe,
            AvatarController avatarController) {
            InitializeContentManager(graphics);
            foreach (var window in UIHook.Instance.Windows) {
                window.Update(universe, avatar);
                if (window.Visible && !window.AlwaysOnTop) {
                    window.Draw(graphics, ref matrix, avatar, universe, avatarController);
                } 
            }
        }

        private static void DrawTop(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity avatar,
            EntityPainter avatarPainter, Universe universe, Timestep timestep) {
            InitializeContentManager(graphics);
            foreach (var window in UIHook.Instance.Windows) {
                window.Update(universe, avatar);
                if (window.Visible && window.AlwaysOnTop) {
                    window.DrawTop(graphics, ref matrix, avatar, avatarPainter, universe, timestep);
                }
            }
        }

        private static void InitializeContentManager(DeviceContext graphics) {
            if (UIHook.Instance.ContentManager == null) {
                UIHook.Instance.ContentManager = graphics.GetPrivateFieldValue<ContentManager>("Content");

                UIHook.Instance.LoadContent();
            }
        }
    }
}
