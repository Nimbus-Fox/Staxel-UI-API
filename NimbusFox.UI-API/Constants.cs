﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NimbusFox.UI_API {
    public static class Constants {
        public static class Fonts {
            public const string MyFirstCrush12 = "nimbusfox.ui.font.myfirstcrush.12";
            public const string MyFirstCrush24 = "nimbusfox.ui.font.myfirstcrush.24";
            public const string MyFirstCrush36 = "nimbusfox.ui.font.myfirstcrush.36";
        }
    }
}
