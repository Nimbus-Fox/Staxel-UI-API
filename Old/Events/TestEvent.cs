﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Community.UI_API.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Rendering;

namespace Community.UI_API.Events {
    public class TestEvent : IEffect {

        public void Dispose() { }
        public bool Completed() {
            return false;
        }

        private UIElement _ui;

        public TestEvent(Blob data) {
            _ui = UIAPI.UIDatabase.GetElement(data.GetString("element"));
            _ui.Show();
            _ui.AquireControl();
        }

        public void Render(Entity entity, EntityPainter painter, Timestep renderTimestep, DeviceContext graphics,
            ref Matrix4F matrix,
            Vector3D renderOrigin, Vector3D position, RenderMode renderMode) {
        }

        public void Stop() { }
        public void Pause() { }
        public void Resume() { }
    }
}
