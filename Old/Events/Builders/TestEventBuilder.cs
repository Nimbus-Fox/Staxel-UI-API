﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Effects;
using Staxel.Logic;

namespace Community.UI_API.Events.Builders {
    public class TestEventBuilder : IEffectBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "community.uiapi.effect.testUI";
        }

        public IEffect Instance(Timestep step, Entity entity, EntityPainter painter, EntityUniverseFacade facade, Blob data,
            EffectDefinition definition, EffectMode mode) {
            return new TestEvent(data);
        }

        public static EffectTrigger BuildTrigger(Blob data) {
            return new EffectTrigger(KindCode(), data);
        }
    }
}
