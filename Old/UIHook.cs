﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace Community.UI_API {
    public class UIHook : IModHookV3 {
        public void Dispose() { }

        public void GameContextInitializeInit() {
        }
        public void GameContextInitializeBefore() {
            if (Process.GetCurrentProcess().ProcessName.StartsWith("Staxel.Client") ||
                Process.GetCurrentProcess().ProcessName.StartsWith("Staxel.ContentBuilder")) {
                UIAPI.UIDatabase.Reload();

                var uiFonts = GameContext.AssetBundleManager.FindByExtension(".uiFont");

                foreach (var item in uiFonts) {
                    var stream = GameContext.ContentLoader.ReadStream(item);
                    stream.Seek(0, SeekOrigin.Begin);
                    var blob = BlobAllocator.Blob(false);
                    blob.LoadJsonStream(stream);
                    UIAPI.UIDatabase.AddFont(item, blob);
                }

                var uiElements = GameContext.AssetBundleManager.FindByExtension(".uiElement");

                foreach (var item in uiElements) {
                    var stream = GameContext.ContentLoader.ReadStream(item);
                    stream.Seek(0, SeekOrigin.Begin);
                    var blob = BlobAllocator.Blob(false);
                    blob.LoadJsonStream(stream);
                    UIAPI.UIDatabase.AddElement(item, blob);
                }
            }
        }
        public void GameContextInitializeAfter() {
        }
        public void GameContextDeinitialize() { }

        public void GameContextReloadBefore() {
        }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() {
        }
        public void ClientContextInitializeBefore() { }

        public void ClientContextInitializeAfter() {
            UIAPI.UIDatabase.SetText();
        }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
