﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace Community.UI_API.Classes {
    public class UIElement : IDisposable {
        protected byte XPercent { get; } = 0;
        protected byte YPercent { get; } = 0;
        protected int XPoint { get; } = 0;
        protected int YPoint { get; } = 0;
        protected int Height { get; set; }
        protected int Width { get; set; }
        protected string Source { get; } = null;
        private Color _backgroundColor { get; } = Color.Transparent;
        private Color _textColor { get; } = Color.Transparent;

        protected Color TextColor {
            get {
                if (_textColor != Color.Transparent) {
                    return _textColor;
                }

                var current = this;

                while (current.Parent != null) {
                    current = current.Parent;

                    if (current._textColor != Color.Transparent) {
                        return current._textColor;
                    }
                }

                return Color.Transparent;
            }
        }

        private UIFont _font { get; } = null;

        protected UIFont Font {
            get {
                if (_font != null) {
                    return _font;
                }

                var current = this;

                while (current.Parent != null) {
                    current = current.Parent;

                    if (current._font != null) {
                        return current._font;
                    }
                }

                return null;
            }
        }

        private double _fontScale { get; } = -1;

        protected double FontScale {
            get {
                if (_fontScale != -1) {
                    return _fontScale;
                }

                var current = this;

                while (current.Parent != null) {
                    current = current.Parent;

                    if (current._fontScale != -1) {
                        return current._fontScale;
                    }
                }

                return 1;
            }
        }

        public UIElement Parent { get; private set; } = null;
        private Blob Blob { get; }
        public bool IsVisible { get; private set; } = false;
        private bool _makeInvisible = false;
        public SpriteBatch SpriteBatch { get; private set; }
        protected bool AutoHeight { get; }
        protected bool AutoWidth { get; }

        protected UIElement TopParent {
            get {
                var current = this;

                while (current.Parent != null) {
                    current = current.Parent;
                }

                return current;
            }
        }

        protected int Y { get; private set; }
        protected int X { get; private set; }

        public event Action OnHide;

        public IReadOnlyList<UIElement> Children { get; private set; } = new List<UIElement>();

        internal UIElement(Blob blob) {
            Blob = blob;
            Height = (int)blob.GetLong("height", 0);
            if (Height == 0) {
                AutoHeight = true;
            }
            Width = (int)blob.GetLong("width", 0);
            if (Width == 0) {
                AutoWidth = true;
            }
            Source = blob.GetString("texture", "");

            if (blob.Contains("textColor")) {
                _textColor = ColorMath.ParseString(blob.GetString("textColor"));
            }

            if (blob.Contains("backgroundColor")) {
                _backgroundColor = ColorMath.ParseString(blob.GetString("backgroundColor"));
            }

            if (blob.Contains("font")) {
                _font = UIAPI.UIDatabase.GetFont(blob.GetString("font"));
            }

            var location = blob.GetBlob("location");

            if (location.TryGetBlobEntry("x", out var x)) {
                if (x.Kind == BlobEntryKind.String) {
                    var xString = x.GetString();

                    if (xString.Contains("%")) {
                        if (byte.TryParse(xString.Replace("%", ""), out var xPercent)) {
                            if (xPercent <= 100) {
                                XPercent = xPercent;
                            }
                        }
                    } else {
                        if (int.TryParse(xString, out var xPoint)) {
                            XPoint = xPoint;
                        }
                    }
                } else if (x.Kind == BlobEntryKind.Int) {
                    if (int.TryParse(x.GetLong().ToString(), out var xPoint)) {
                        XPoint = xPoint;
                    }
                }
            }

            if (location.TryGetBlobEntry("y", out var y)) {
                if (y.Kind == BlobEntryKind.String) {
                    var yString = y.GetString();

                    if (yString.Contains("%")) {
                        if (byte.TryParse(yString.Replace("%", ""), out var yPercent)) {
                            if (yPercent <= 100) {
                                YPercent = yPercent;
                            }
                        }
                    } else {
                        if (int.TryParse(yString, out var yPoint)) {
                            YPoint = yPoint;
                        }
                    }
                } else if (x.Kind == BlobEntryKind.Int) {
                    if (int.TryParse(y.GetLong().ToString(), out var yPoint)) {
                        YPoint = yPoint;
                    }
                }
            }

            if (blob.Contains("fontScale")) {
                if (blob.GetDouble("fontScale") > 0) {
                    _fontScale = blob.GetDouble("fontScale");
                }
            }
        }

        public virtual void RenderBefore(DeviceContext context) {
            var state = Keyboard.GetState();

            if (state.IsKeyDown(Keys.Escape)) {
                Hide();
            }

            if (!context.IsActive()) {
                Hide();
                ClientContext.WebOverlayRenderer.AcquireInputControl();
            }

            if (!IsVisible) {
                return;
            }

            if (TopParent == this) {
                if (SpriteBatch == null) {
                    SpriteBatch = new SpriteBatch(context.Graphics.GraphicsDevice);
                }
                context.PushRenderState();
                context.Graphics.GraphicsDevice.BlendState = BlendState.NonPremultiplied;
                context.DepthBuffer(false, false);
                context.PushShader();
                context.SetShader(context.GetShader(Constants.ColorCorrectionActive ? "OverlayCC" : "Overlay"));
                SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp, DepthStencilState.None, new RasterizerState() { MultiSampleAntiAlias = true });
            }

            var graphics = context.Graphics.GraphicsDevice;

            Font.Render(graphics);

            if (YPercent > 0) {
                Y = Parent?.Height ?? graphics.Viewport.Height;

                Y -= (int)Math.Floor(Y * (1 - (double)YPercent / 100));
                Y -= (int)Math.Floor(Height * (1 - (double)YPercent / 100));

                if (Parent != null) {
                    Y += Parent.Y;
                }

                if (Y < 0) {
                    Y = Parent?.Y ?? 0;
                }
            } else {
                if (YPoint > 0) {
                    Y = Parent?.Y ?? 0;
                    Y += YPoint;
                }
            }

            if (XPercent > 0) {
                X = Parent?.Width ?? graphics.Viewport.Width;

                X -= (int)Math.Floor(X * (1 - (double)XPercent / 100));
                X -= (int)Math.Floor(Width * (1 - (double)XPercent / 100));

                if (Parent != null) {
                    X += Parent.X;
                }

                if (X < 0) {
                    X = Parent?.X ?? 0;
                }
            } else {
                if (XPoint > 0) {
                    X = Parent?.X ?? 0;
                    X += XPoint;
                }
            }

            foreach (var child in Children) {
                child.RenderBefore(context);
            }
        }

        public virtual void Render(DeviceContext context) {
            if (!IsVisible) {
                return;
            }

            foreach (var child in Children) {
                child.Render(context);
            }
        }

        public virtual void RenderAfter(DeviceContext context) {
            if (!IsVisible) {
                return;
            }

            Font.Render(context.Graphics.GraphicsDevice);

            if (TopParent == this) {
                SpriteBatch.End();
                context.PopShader();
                context.PopRenderState();
            }

            foreach (var child in Children) {
                child.RenderAfter(context);
            }

            if (_makeInvisible) {
                IsVisible = false;
                _makeInvisible = false;
            }
        }

        public void RegisterChildren() {
            var list = new List<UIElement>();

            if (Blob.Contains("children")) {
                if (Blob.KeyValueIteratable["children"].Kind == BlobEntryKind.List) {
                    foreach (var child in Blob.GetList("children")) {
                        if (child.Kind == BlobEntryKind.String) {
                            list.Add(UIAPI.UIDatabase.GetElement(child.GetString()));
                            list.Last().Parent = this;
                        }
                    }
                } else if (Blob.KeyValueIteratable["children"].Kind == BlobEntryKind.String) {
                    list.Add(UIAPI.UIDatabase.GetElement(Blob.GetString("children")));
                    list.Last().Parent = this;
                }
            }

            Children = list;
        }

        public virtual void Dispose() {
            ReleaseControl();
        }

        public void AquireControl() {
            ClientContext.WebOverlayRenderer.AcquireInputControl();
        }

        public void ReleaseControl() {
            ClientContext.WebOverlayRenderer.ReleaseInputControl();
        }

        public void Show() {
            IsVisible = true;

            foreach (var child in Children) {
                child.Show();
            }
        }

        public void Hide() {
            ReleaseControl();
            _makeInvisible = true;

            foreach (var child in Children) {
                child.Hide();
            }

            OnHide?.Invoke();
        }

        protected Color GetTextColor() {
            if (TextColor != Color.Transparent) {
                return TextColor;
            }

            var current = this;

            while (current.Parent != null) {
                current = current.Parent;

                if (current.TextColor != Color.Transparent) {
                    return current.TextColor;
                }
            }

            return Color.Transparent;
        }
    }
}
