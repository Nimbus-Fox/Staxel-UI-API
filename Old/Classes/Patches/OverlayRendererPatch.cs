﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harmony;
using Plukit.Base;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace Community.UI_API.Classes.Patches {
    [HarmonyPatch(typeof(OverlayRenderer), "DrawTop", typeof(DeviceContext), typeof(Matrix4F), typeof(Vector3D), typeof(Entity), typeof(EntityPainter), typeof(Universe), typeof(Timestep))]
    public static class OverlayRendererPatch {
        public static void Postfix(DeviceContext graphics, Matrix4F matrix, Vector3D renderOrigin, Entity avatar,
            EntityPainter avatarPainter, Universe universe, Timestep timestep) {
            UIAPI.UIDatabase.DrawUI(graphics);
        }
    }
}
