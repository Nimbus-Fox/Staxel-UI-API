﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;

namespace Community.UI_API.Classes.Elements {
    public class UIContainerElement : UIElement {
        public const string Kind = "container";
        protected Texture2D Texture = null;
        internal UIContainerElement(Blob blob) : base(blob) { }

        public override void RenderBefore(DeviceContext context) {
            if (!Source.IsNullOrEmpty()) {
                if (Texture == null) {
                    var stream = new FileStream(Path.Combine(GameContext.ContentLoader.RootDirectory, Source), FileMode.Open);
                    stream.Seek(0, SeekOrigin.Begin);
                    Texture = Texture2D.FromStream(context.Graphics.GraphicsDevice, stream);
                    stream.Dispose();
                }
            }
            base.RenderBefore(context);
        }

        public override void Render(DeviceContext context) {
            if (!IsVisible) {
                return;
            }

            if (Texture != null) {
                TopParent.SpriteBatch.Draw(Texture, new Rectangle(X, Y, Width, Height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0f);
            }

            base.Render(context);
        }

        public override void Dispose() {
            Texture?.Dispose();
            base.Dispose();
        }
    }
}
