﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Plukit.Base;
using Staxel;
using Staxel.Draw;

namespace Community.UI_API.Classes.Elements {
    public class UILabelElement : UIElement {
        public const string Kind = "label";
        private readonly List<Text> _text = new List<Text>();
        private readonly string _languageCode;
        protected int MaxHeight { get; } = int.MaxValue;
        protected int MaxWidth { get; } = int.MaxValue;
        protected string RenderDirection { get; } = "ltr";
        protected bool WordWrap { get; } = true;

        internal UILabelElement(Blob blob) : base(blob) {
            _languageCode = blob.GetString("content");

            if (blob.Contains("maxHeight")) {
                if (int.TryParse(blob.GetLong("maxHeight").ToString(), out var maxHeight)) {
                    if (maxHeight > 0) {
                        MaxHeight = maxHeight;
                    }
                }
            }

            if (blob.Contains("maxWidth")) {
                if (int.TryParse(blob.GetLong("maxWidth").ToString(), out var maxWidth)) {
                    if (maxWidth > 0) {
                        MaxWidth = maxWidth;
                    }
                }
            }

            if (blob.Contains("renderDirection")) {
                var direction = blob.GetString("renderDirection");

                if (direction == "rtl") {
                    RenderDirection = direction;
                }
            }

            if (blob.Contains("wordWrap")) {
                WordWrap = blob.GetBool("wordWrap");
            }
        }

        private class Text {
            public Rectangle CharacterPosition;
            public Rectangle ScreenPosition;
            public Vector2I Offset;
            public char Character;
        }

        public override void RenderBefore(DeviceContext context) {
            var offset = Vector2I.Zero;

            var word = new List<Text>();
            
            var maxWidth = 0;

            var canNewLine = true;

            var firstLine = true;

            foreach (var item in _text) {
                if (offset.X > MaxWidth || word.Sum(x => x.Offset.X) > MaxWidth) {
                    offset.X = 0;
                    if (canNewLine) {
                        if (!firstLine) {
                            offset.Y += Font.Dimensions.Y;
                        }

                        firstLine = false;
                        canNewLine = false;
                    }

                    if (WordWrap) {
                        foreach (var letter in word) {
                            letter.Offset.X = offset.X;
                            letter.Offset.Y = offset.Y;

                            offset.X += (int) Math.Round(letter.CharacterPosition.Width * FontScale);
                        }
                    } else {
                        canNewLine = true;
                    }
                }

                if (item.Character == ' ' || item.Character == '\n') {
                    word.Clear();
                    canNewLine = true;
                } else {
                    word.Add(item);
                }

                if (offset.X > maxWidth) {
                    maxWidth = offset.X;
                }

                item.Offset.X = offset.X;
                item.Offset.Y = offset.Y;

                offset.X += (int)Math.Round(item.CharacterPosition.Width * FontScale);
            }

            if (AutoHeight) {
                Height = (int)Math.Round((offset.Y + Font.Dimensions.Y) * FontScale);
            }

            Height = Height > MaxHeight ? MaxWidth : Height;

            if (AutoWidth) {
                Width = maxWidth;
            }

            Width = Width > MaxWidth ? MaxWidth : Width;

            base.RenderBefore(context);

            foreach (var item in _text) {
                item.ScreenPosition.X = X + item.Offset.X;
                if (RenderDirection == "rtl") {
                    item.ScreenPosition.X -= Width;
                }
                item.ScreenPosition.Y = Y + (int)Math.Round(item.Offset.Y * FontScale);
            }
        }

        public override void Render(DeviceContext context) {
            foreach (var item in _text) {
                TopParent.SpriteBatch.Draw(Font.Texture, item.ScreenPosition, item.CharacterPosition, GetTextColor());
            }

            base.Render(context);
        }

        internal void SetText() {
            _text.Clear();

            var text = ClientContext.LanguageDatabase.GetTranslationString(_languageCode);

            var offset = Vector2I.Zero;

            foreach (var c in text) {
                var character = Font.GetCharacter(c);

                if (offset.X > Width) {
                    offset.X = 0;
                    offset.Y++;
                }

                var item = new Text {
                    ScreenPosition = new Rectangle(X + offset.X, Y + (int)Math.Round(Font.Dimensions.Y * FontScale) * offset.Y, (int)Math.Round(character.Width * FontScale), (int)Math.Round(Font.Dimensions.Y * FontScale)),
                    CharacterPosition = character,
                    Offset = new Vector2I(),
                    Character = c
                };

                offset.X += character.Width;

                _text.Add(item);
            }
        }
    }
}
