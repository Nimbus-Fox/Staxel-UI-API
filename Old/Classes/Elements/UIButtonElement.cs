﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace Community.UI_API.Classes.Elements {
    public class UIButtonElement : UIElement {
        public const string Kind = "button";
        internal UIButtonElement(Blob blob) : base(blob) { }
    }
}
