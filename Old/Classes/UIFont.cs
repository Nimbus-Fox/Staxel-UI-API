﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Plukit.Base;
using Staxel;
using Staxel.Core;

namespace Community.UI_API.Classes {
    public class UIFont {
        public string Code { get; }
        private Dictionary<char, Rectangle> _characters = new Dictionary<char, Rectangle>();
        private string _texture { get; }
        public Texture2D Texture { get; private set; }
        public Vector2I Dimensions { get; } = new Vector2I(32, 32);
        private int _height { get; }
        private int _width { get; }
        private Color _ignoreColor { get; } = Color.Transparent;

        public UIFont(Blob blob) {
            _texture = blob.GetString("texture");

            if (blob.Contains("dimensions")) {
                Dimensions = blob.FetchBlob("dimensions").GetVector2I();
            }

            if (blob.Contains("characters")) {
                foreach (var entry in blob.FetchBlob("characters").KeyValueIteratable) {
                    if (char.TryParse(entry.Key, out var character)) {
                        var rect = new Rectangle((int)entry.Value.Blob().GetLong("x") * Dimensions.X, (int)entry.Value.Blob().GetLong("y") * Dimensions.Y,
                            (int)entry.Value.Blob().GetLong("width", Dimensions.X), (int)entry.Value.Blob().GetLong("height", Dimensions.Y));
                        _characters.Add(character, rect);
                    }
                }
            }

            if (blob.Contains("ignoreColor")) {
                _ignoreColor = ColorMath.ParseString(blob.GetString("ignoreColor"));
            }

            Code = blob.GetString("code");
            _height = (int)blob.GetLong("height");
            _width = (int)blob.GetLong("width");

            string text;

            try {
                text = ClientContext.LanguageDatabase.GetTranslationString("community.uiapi.message.fontResult");
            } catch {
                text = "UIAPI: {0} font has {1} characters";
            }

            Logger.WriteLine(string.Format(text, Code, _characters.Count));
        }

        public Rectangle GetCharacter(char character) {
            return _characters.ContainsKey(character) ? _characters[character] : _characters.First().Value;
        }

        public void Render(GraphicsDevice graphics) {
            if (Texture == null) {
                var stream = new FileStream(Path.Combine(GameContext.ContentLoader.RootDirectory, _texture), FileMode.Open);
                stream.Seek(0, SeekOrigin.Begin);
                Texture = Texture2D.FromStream(graphics, stream);

                var colorData = new Color[Texture.Height * Texture.Width];

                Texture.GetData(colorData);

                for (var i = 0; i < colorData.Length; i++) {
                    if (colorData[i] == _ignoreColor) {
                        colorData[i] = Color.Transparent;
                    }
                }

                Texture.SetData(colorData);

                stream.Dispose();
            }
        }
    }
}
