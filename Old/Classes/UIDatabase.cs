﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Community.UI_API.Classes.Elements;
using Harmony;
using Plukit.Base;
using Staxel;
using Staxel.Draw;

namespace Community.UI_API.Classes {
    public class UIDatabase {
        private readonly Dictionary<string, UIElement> _elementDatabase = new Dictionary<string, UIElement>();
        private readonly Dictionary<string, UIFont> _fontDataase = new Dictionary<string, UIFont>();

        static UIDatabase() {
            var harmony = HarmonyInstance.Create("community.uiapi");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

        public UIElement GetElement(string code) {
            var element = _elementDatabase.ContainsKey(code) ? _elementDatabase[code] : null;

            element?.RegisterChildren();

            return element;
        }

        public UIFont GetFont(string code) {
            return _fontDataase.ContainsKey(code) ? _fontDataase[code] : _fontDataase.First().Value;
        }

        internal void AddElement(string file, Blob blob) {
            var code = blob.GetString("code");
            var type = blob.GetString("type");

            if (_elementDatabase.ContainsKey(code)) {
                string text;
                try {
                    text = ClientContext.LanguageDatabase.GetTranslationString(
                        "community.uiapi.exceptions.elementExists");
                } catch {
                    text = "An element with the code '{0}' already exists";
                }

                throw new Exceptions.UIElementExistsException(string.Format(text, code));
            }

            switch (type) {
                case UIContainerElement.Kind:
                    _elementDatabase.Add(code, new UIContainerElement(blob));
                    break;
                case UILabelElement.Kind:
                    _elementDatabase.Add(code, new UILabelElement(blob));
                    break;
                default:
                    string text2;
                    try {
                        text2 = ClientContext.LanguageDatabase.GetTranslationString(
                            "community.uiapi.exceptions.invalidType");
                    } catch {
                        text2 = "{0} has an invalid type '{1}'";
                    }

                    throw new Exceptions.UIElementException(string.Format(text2, file, type));
            }
        }

        internal void AddFont(string file, Blob blob) {
            var code = blob.GetString("code");

            if (_fontDataase.ContainsKey(code)) {
                string text;
                try {
                    text = ClientContext.LanguageDatabase.GetTranslationString("community.uiapi.exceptions.fontExists");
                } catch {
                    text = "A font with the code '{0}' already exists";
                }
                throw new Exceptions.UIFontExistsException(string.Format(text, code));
            }
            
            _fontDataase.Add(code, new UIFont(blob));
        }

        internal void Reload() {
            _elementDatabase.Clear();
            _fontDataase.Clear();
        }

        internal void SetText() {
            foreach (var item in _elementDatabase) {
                if (item.Value is UILabelElement label) {
                    label.SetText();
                }
            }
        }

        internal void DrawUI(DeviceContext graphics) {
            foreach (var element in _elementDatabase.Values.Where(x => x.Parent == null && x.IsVisible)) {
                element.RenderBefore(graphics);
                element.Render(graphics);
                element.RenderAfter(graphics);
            }
        }
    }
}
