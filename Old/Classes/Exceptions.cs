﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Community.UI_API.Classes {
    public class Exceptions {
        public class UIElementException : Exception {
            public UIElementException() : base() { }

            public UIElementException(string message) : base(message) { }

            public UIElementException(string message, Exception innerException) : base(message, innerException) { }
        }

        public class UIElementExistsException : Exception {
            public UIElementExistsException() : base() { }

            public UIElementExistsException(string message) : base(message) { }

            public UIElementExistsException(string message, Exception innerException) : base(message, innerException) { }
        }

        public class UIFontExistsException : Exception {
            public UIFontExistsException() : base() { }

            public UIFontExistsException(string message) : base(message) { }

            public UIFontExistsException(string message, Exception innerException) : base(message, innerException) { }
        }
    }
}
