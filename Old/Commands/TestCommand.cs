﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Community.UI_API.Events;
using Community.UI_API.Events.Builders;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Effects;
using Staxel.Server;

namespace Community.UI_API.Commands {
    public class TestCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (bits.Length < 2) {
                return "";
            }

            if (api.TryGetEntity(connection.ConnectionEntityId, out var player)) {
                var effect = BlobAllocator.Blob(true);
                effect.SetString("element", bits[1]);
                player.Effects.Trigger(TestEventBuilder.BuildTrigger(effect));
            }

            return "";
        }

        public string Kind => "uitest";
        public string Usage => "";
        public bool Public => false;
    }
}
